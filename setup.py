"""
Instructions for pip install.
"""
import os

from setuptools import setup, find_packages

from dock_db import __version__

# Meta-information
_package_name = 'dock_db'

setup(
    name=_package_name,
    version=__version__,
    url='https://gitlab.com/hd-public/dock-db/dockdb-usage',
    author='Harmonic Discovery',
    author_email='guru@harmonicdiscovery.com',
    packages=find_packages(include=['dock_db', 'dock_db.*']),
    install_requires = [
        'attrs>=21.4.0',
        'boto3==1.24.49',
        'botocore>=1.27.59',
        'certifi>=2021.10.8',
        'greenlet>=1.1.2',
        'importlib-metadata>=4.10.1',
        'iniconfig>=1.1.1',
        'jinja2>=3.0',
        'numpy>=1.21.6',
        'nglview==3.0.6',
        'packaging>=21.3',
        'pandas<2.0.0',
        'pluggy>=1.0.0',
        'py>=1.11.0',
        'pyparsing>=3.0.7',
        'pytest>=7.0.0',
        'python-dotenv==1.0.0',
        'rdkit>=2022.9.2',
        'seaborn==0.11.2',
        'SQLAlchemy>=1.4.31',
        'tomli>=2.0.0',
        'tqdm>=4.62.3',
        'typing_extensions>=4.0.1',
        'zipp>=3.7.0',
    ]
)
