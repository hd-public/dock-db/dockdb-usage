import os
import boto3
import psycopg2
from dotenv import load_dotenv


def read_file_from_s3(bucket, key):
    """
    Read the kinase dataset from s3 bucket

    Returns:
        obj
    """
    assert (os.environ['AWS_ACCESS_KEY_ID'] is not None) and (os.environ['AWS_SECRET_ACCESS_KEY']
                                                              is not None), 'need to set environment variables AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY'
    print('attemping to connect to s3')

    REGION = 'us-east-2'
    BUCKET_NAME = bucket
    KEY = key

    s3c = boto3.client(
        's3',
        region_name=REGION,
        aws_access_key_id=os.environ['AWS_ACCESS_KEY_ID'],
        aws_secret_access_key=os.environ['AWS_SECRET_ACCESS_KEY']
    )
    obj = s3c.get_object(Bucket=BUCKET_NAME, Key=KEY)

    return obj


def connect_to_db(env='./.env'):
    """
    Connect to the DockDB database

    Parameters
    ----------
    env: string
        path/to/.env file that contains the security information necessary to login
    """
    load_dotenv(dotenv_path=env)

    conn = psycopg2.connect(
        host=os.getenv("DBhost"),
        database=os.getenv("DB"),
        user=os.getenv("USER"),
        port= os.getenv("DBport"),
        password=os.getenv("DBpwd"))
    curs = conn.cursor()
    print('DB connection established')
    return curs
