import os
import io
from dock_db.utils import read_file_from_s3
import boto3
import ipywidgets as widgets
from ipywidgets import interact, interact_manual
import pandas as pd
import numpy as np
import nglview as nv
from rdkit import Chem



from plip.exchange.report import BindingSiteReport
from plip.structure.preparation import PDBComplex


def _combine_molecules(protein_pdb_path, ligand_pdb_path):
    """
    Comdined docked ligand pose PDB file and protein PDB file into a single PDB file.

    Paramenters
    -----------
    protein_pdb_path: string
        Location of protein PDB file
    
    ligand_pdb_path: string
        Location of ligand PDB file 

    Returns
    -------
    None
    """

    # Read protein and ligand PDB files
    protein_lines = open(protein_pdb_path, 'r').readlines()
    ligand_lines = open(ligand_pdb_path, 'r').readlines()

    combined_lines = []
    
    # Copy protein lines
    for line in protein_lines:
        if line.startswith('ATOM'):
            combined_lines.append(line)
    combined_lines.append('TER\n')

    # Adjust atom serial numbers for ligand
    last_serial = int(combined_lines[-2][6:11])
    prefix = combined_lines[-2][0:6]
    last_res = int(combined_lines[-2][23:26])

    for line in ligand_lines:
        if line.startswith('ATOM') or line.startswith('HETATM'):
            serial = last_serial + int(line[6:11])
            res = '999'
            chain = ' Z '
            line = prefix + f"{serial:5}" + line[11:20]+chain +f"{res}" + line[26:]
            combined_lines.append(line)
    combined_lines.append('END')
    
    # Write combined PDB file
    with open('tmp_combined.pdb', 'w') as output_file:
        output_file.writelines(combined_lines)
    output_file.close()

    try:
        os.remove(protein_pdb_path)
    except Exception as e:
        print(f"Error deleting the file: {e}")


def make_complex_file(lig_mol, pdb_file):
    """
    Comdined docked ligand pose PDB file and protein PDB file into a single PDB file.

    Paramenters
    -----------
    lig_mol: rdkit mol obj
        RDKit mol object of the docked ligand pose
    
    ligand_pdb_path: string
        Location of ligand PDB file 

    Returns
    -------
    None
    """
    try:
        os.remove('tmp_ligand.pdb')
    except Exception as e:
        print(f"Error deleting the file: {e}")

    # Create a PDB writer
    pdb_writer = Chem.PDBWriter('tmp_ligand.pdb')  # Replace 'output.pdb' with the desired output PDB file name
    # Write the molecule to the PDB file
    pdb_writer.write(lig_mol)

    # Close the PDB writer
    pdb_writer.close()

    _combine_molecules(pdb_file, 'tmp_ligand.pdb')


def retrieve_plip_interactions():
    """
    Retrieves the interactions from PLIP.
        Code adjusted from https://projects.volkamerlab.org/teachopencadd/talktorials/T016_protein_ligand_interactions.html

    Returns
    -------
    dict :
        A dictionary of the binding sites and the interactions.
    """
    
    protlig = PDBComplex()
    protlig.load_pdb('tmp_combined.pdb')  # load the pdb file
    for ligand in protlig.ligands:
        protlig.characterize_complex(ligand)  # find ligands and analyze interactions
        
    sites = {}
    # loop over binding sites
    for key, site in sorted(protlig.interaction_sets.items()):
        binding_site = BindingSiteReport(site)  # collect data about interactions
        keys = (
            "hydrophobic",
            "hbond",
            "waterbridge",
            "saltbridge",
            "pistacking",
            "pication",
            "halogen",
            "metal",
        )
        interactions = {
            k: [getattr(binding_site, k + "_features")] + getattr(binding_site, k + "_info")
            for k in keys
        }
        sites[key] = interactions

    return sites


class Rendering:
    """
    This class creates a NGL self.viewer object to visualize  the results of docking 
    experiment by rendering the structures or docking poses
    """

    def __init__(self, storage='s3'):
        """
        Parameters
        ----------
        storage: string
            Default s3. insert anything else .eg. local if your files are stored in your computer
            If you are storing your files in a cloud system other than AWS or you use scp you will
            need to modify the code to accomodate the pulling of the data. 
        """
        self.storage = storage

    color_map = {
    "hydrophobic": [0.90, 0.10, 0.29],
    "hbond": [0.26, 0.83, 0.96],
    "waterbridge": [1.00, 0.88, 0.10],
    "saltbridge": [0.67, 1.00, 0.76],
    "pistacking": [0.75, 0.94, 0.27],
    "pication": [0.27, 0.60, 0.56],
    "halogen": [0.94, 0.20, 0.90],
    "metal": [0.90, 0.75, 1.00]}

    viewer = nv.NGLWidget()
    views = {}


    def _show_interactions_3d(self, highlight_interaction_colors=color_map):
        """
        3D visualization of protein-ligand interactions. 
        Code adapted from volkamer tutorial (https://projects.volkamerlab.org/teachopencadd/talktorials/T016_protein_ligand_interactions.html)

        Parameters
        ----------
        highlight_interaction_colors : dict
            The colors used to highlight the different interaction types.

        Returns
        -------
        NGL self.viewer with explicit interactions given by PLIP.
        """

        selected_site_interactions = retrieve_plip_interactions()
        selected_site_interactions = selected_site_interactions['UNL:Z:999']
        interacting_residues = []
        for interaction_type, interaction_list in selected_site_interactions.items():
            color = highlight_interaction_colors[interaction_type]
            if len(interaction_list) == 1:
                continue
            df_interactions = pd.DataFrame.from_records(
                interaction_list[1:], columns=interaction_list[0]
            )
            for _, interaction in df_interactions.iterrows():
                name = interaction_type

                # add cylinder between ligand and protein coordinate
                self.viewer.shape.add_cylinder(
                    interaction["LIGCOO"],
                    interaction["PROTCOO"],
                    color,
                    [0.1],
                    name,
                )
                interacting_residues.append(interaction["RESNR"])

        # Display interacting residues
        res_sele = " or ".join([f"({r} and not _H)" for r in interacting_residues])
        res_sele_nc = " or ".join([f"({r} and ((_O) or (_N) or (_S)))" for r in interacting_residues])
        self.viewer.component_1.add_ball_and_stick(sele=res_sele, color="#B2BEB5", aspectRatio=1.5)
        self.viewer.component_1.add_ball_and_stick(sele=res_sele_nc, colorScheme="element", aspectRatio=1.5)

        # Center on ligand
        self.viewer.center("ligand")

        return self.viewer


    def _name_widget_list(self, ligand):
        """
        Create a dictionary where the keys are the pose number for the dock ligand and the value are the widget of the rendered docked pose

        Paramters
        ---------
        self

        ligand: string
            Ligand name
        
        Returns
        -------
        filled_views: dict
        """
        names = [ligand+': Pose '+ str(x+1) for x in range(len(self.views))]
        filled_views = dict(zip(names, self.views))

        return filled_views
    

    def visualize_structure(self, structure_df, pdb_id):
        """
        3D visualization of protein structure using NGLVIEW . 

        Parameters
        ----------
        structure_df: Pandas DataFrame. 
            Contains the location of the protein structures to pull

        pdb_id: string. 
            PDB id of the protein

        Returns
        -------
        NGL self.viewer with explicit interactions given by PLIP.
        """

        # Retrieve the location of the protein structure
        location = structure_df[structure_df['pdb_id'] == pdb_id]['structure_location'].values[0]
        ts = location.split('/')

        ### CHANGE: remove KLIF_fingerprints from line 240
        if self.storage == 's3':
        # Pull the structure 
            representative_pdb_obj = read_file_from_s3(ts[2], 'KLIFS_fingerprints/'+ts[3]+'/'+ts[4]+'/'+ts[5]+'/'+ts[6]+'/complex.pdb')
            representative_pdb = pd.read_csv(io.BytesIO(representative_pdb_obj['Body'].read()), encoding='utf8', low_memory=False, skiprows=37 )

            with open('tmp.pdb', 'w') as f:
               f.write(representative_pdb.to_string(index=False)) 

        else:
            print(os.getcwd())
            os.system('cp '+location+' tmp.pdb')

        self.viewer.add_component('tmp.pdb')
        self.viewer.component_0.remove_cartoon()
        self.viewer.component_0.add_cartoon(color='#f7e7ce')

        return self.viewer


    def visualize_docked(self, structure_df, results_loc, docked_df, pdb_id, ligand):
        """
        Visulize docked ligand poses in protein structures and highlight the interactions using nglviewer and plip

        Parameters
        ----------
        structure_df: Pandas DataFrame. 
            Contains the location of the protein structures to pull
        
        results_loc: Pandas DataFrame. 
            Contains the location of the docked ligand sdf file to pull

        pdb_id: string. 
            PDB id of the protein

        ligand: string. 
            Ligand name as in cmp_id in DockDB

        Returns
        -------
        views: list. List of NGLVIEWER, one for each docking pose of the ligand 
        """

        # Find the correct docking results file for the given compound/pdb 
        res_loc = results_loc['results_location'].values
        doc_loc = docked_df['docked_pose_location'].values
        docked_poses = res_loc+doc_loc

        for dp in np.unique(docked_poses):
            if pdb_id.lower() in docked_poses[0]: 
                docked_pose_location = docked_poses[0]
                break
            else:
                continue

        # Download docking results
        ts = docked_pose_location.split('/')
        if self.storage =='s3':
            s3 = boto3.client('s3')
            s3.download_file(ts[2], ts[3]+'/'+ts[4]+'/'+ts[5], 'tmp.sdf')
        else:
            os.system('cp '+docked_pose_location+' tmp.sdf')
        
        # Find index of molecule to visualize
        supplier = Chem.SDMolSupplier('tmp.sdf')
        molecules = [mol for mol in supplier if mol is not None]
        names = [x.GetProp("_Name") for x in molecules]

        for i in range(len(names)):
            if ligand in names[i]: 
                indx = i
                break
            else:
                continue

        # Visualize 
        view = self.visualize_structure(structure_df, pdb_id)
        pose_n = docked_df['pose_id'].unique()
        self.views = []
        for pose in range(len(pose_n)):
            try:
                os.remove('tmp_combined.pdb')
            except Exception as e:
                print(f"Error deleting the file: {e}")

            make_complex_file(molecules[indx+pose], 'tmp.pdb')
            self.viewer = nv.NGLWidget()
            self.viewer = nv.show_rdkit(molecules[indx+pose])
            self.viewer.add_component('tmp_combined.pdb')

            # Adjust aesthetic 
            self.viewer.component_1.remove_ball_and_stick()
            self.viewer.component_1.remove_cartoon()
            self.viewer.component_1.add_cartoon(color='#f7e7ce')
            pose_view = self._show_interactions_3d()
            self.views.append(pose_view)

        # Render 
        self.views = self._name_widget_list(ligand)
        @interact(selected_widget=self.views)
        def show_poses(selected_widget):
            return selected_widget
