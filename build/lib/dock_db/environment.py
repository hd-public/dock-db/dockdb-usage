"""
Code for connecting to the databse via Python.

Typical usage:

from dock_db.environment import session_scope

with session_scope() as sesssion:
    # Everything inside of this block will be executed as a single SQL transaction.
    # If there is an exception inside of this block, the transaction will be rolled
    # back.

    rows = session.execute('SELECT * FROM compounds').all();
    # ... other commands to read from and modify the database
"""

from contextlib import contextmanager
import os

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


engine = create_engine(os.environ.get('DOCK_DATABASE_URL'))
Session = sessionmaker(bind=engine)


@contextmanager
def session_scope():
    session = Session()
    try:
        yield session
        session.commit()
    except:
        session.rollback()
        raise
    finally:
        session.close()
