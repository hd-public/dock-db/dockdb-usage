import numpy as np
import pandas as pd
import seaborn as sns
from sklearn import metrics
import matplotlib.pyplot as plt

def calculate_enrichments(df):
    """
    Sorts the dataframe using the docking binding affinity and calculate the enrichment
    """

    df = df.copy()
    df['docking_score'] = df['docking_score'].astype(float)
    df_sorted = df.sort_values(by=['docking_score'])
    
    efs_act = [] 
    perc_tot = []
    tot_act = len(df_sorted[df_sorted['cmp_id'].str.contains('HD')])
    tot_comp = len(df_sorted)

    for i in range(1, len(df_sorted)+1):
        df_sub = df_sorted[:i]
        num_act = len(df_sub[df_sub['cmp_id'].str.contains('HD')])

        ef_act = num_act / tot_act * 100
        perc = i/tot_comp * 100
       
        efs_act.append(ef_act)
        perc_tot.append(perc)
        
    return efs_act, perc_tot


def plot_enrichment(structures, results_df):
    """
    Plot enrichment curve of actives vs total number of molecules

    Parameters
    ----------
    structures: list
        List of PDB structures that have been used in the docking study
    
    result_df: pandas DataFrame
        DataFrame from DockDB results table
    
    Returns
    -------
    plt: matplotlib.pyplot plot
    """
    auc = []
    sns.set_context('notebook', font_scale=1)

    plt.figure()
    sns.despine()

    for pdb in structures:
        tmp_df = results_df[results_df['pdb_id'] == pdb]
        efs_act, perc_tot = calculate_enrichments(tmp_df)
        auc = metrics.auc(np.asarray(np.log(perc_tot)), np.asarray(efs_act))
        sns.lineplot(x=np.log(perc_tot), y=efs_act, label=pdb+', LogAUC = '+str(round(auc,2)))

    plt.axvspan(0, 1, alpha=.3, color='gray')
    liny = np.linspace(1,100, len(perc_tot))
    linx = np.linspace(1,100, len(perc_tot))
    ax = sns.lineplot(x=np.log(linx), y=liny, linestyle='dashed', color='gray')
    plt.xscale('log')
    plt.xlabel('%  of total compounds (log)')
    plt.ylabel('% of active compounds')

    return plt