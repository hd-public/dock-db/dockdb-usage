"""
Code for generating compound IDs from database.

Note that calling these functions increments the Postgres counters that track
the next available ID, regardless of whether you use the ID you retrieved.

A user will need write permission to use these functions.
"""


from sqlalchemy.schema import Sequence


pose_id_seq = Sequence('pose_id_seq')


def new_pose_id(session):
    unique_id = session.execute(pose_id_seq)
    full_numeric_id = int(1E04) + unique_id
    assert unique_id < 2E04, 'Out of open pose IDs'
    return 'POSE{:0>4}'.format(full_numeric_id)
