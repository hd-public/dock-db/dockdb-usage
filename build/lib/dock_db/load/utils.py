"""
Utilities for batch loading the database.
"""


import argparse
from copy import deepcopy
from difflib import diff_bytes
from unittest import result
import re


def parse_args():
    """Parse command line arguments common across load scripts

    Returns (filename, allow_update)
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('filename', help='CSV file containing the data to load')
    parser.add_argument('--allow_update', action='store_true', default=False,
                        help='If true, duplicate items will be updated based on the data in the CSV.')
    args = parser.parse_args()

    return args.filename, args.allow_update


def get_structure_id(pdb_id, structure_type, s3_loc, session):
    """Returns the structure_id or None if the structure_id is not in the structures table."""
    rows = session.execute("SELECT structure_id FROM structures "
        "WHERE "
        "  pdb_id = :pdb_id AND "
        "  structure_type = :structure_type AND "
        "  structure_location = :s3_loc",
        {
            'pdb_id': pdb_id,
            'structure_type': structure_type,
            's3_loc': s3_loc
        }
    ).all()
    #rows = session.execute("SELECT structure_id FROM structures WHERE pdb_id = :x AND", {'x': pdb_id}).all()
    if len(rows) < 1:
        return None
    assert len(rows) == 1, 'Non-unique structure_id'
    return rows[0]['structure_id']


def get_cmp_hd_id(cmp_hd_id,s3_loc, session):
    """Returns the cmp_hd_id or None if the cmp_hd_id is not in the compounds table."""
    rows = session.execute("SELECT cmp_hd_id FROM compounds"
        " WHERE " 
        " cmp_hd_id = :cmp_hd_id AND "
        " cmp_location = :s3_loc", 
        {'cmp_hd_id': cmp_hd_id,
        's3_loc': s3_loc
        }
        ).all()
    if len(rows) < 1:
        return None
    else:
        print(s3_loc)
    assert len(rows) == 1, 'Non-unique cmp_hd_id'
    return rows[0]['cmp_hd_id']


def get_decoy_id(decoy_smiles,s3_loc, session):
    """Returns the decoy_id or None if the decoy_id is not in the decoys table."""
    rows = session.execute("SELECT decoy_id FROM decoys"
        " WHERE " 
        " decoy_smiles = :decoy_smiles AND "
        " decoy_location = :s3_loc", 
        {'decoy_smiles': decoy_smiles,
        's3_loc': s3_loc
        }
        ).all()
    if len(rows) < 1:
        return None
    assert len(rows) == 1, 'Non-unique decoy_id'
    return rows[0]['decoy_id']


def get_job_id(job_id, session):
    """Returns the job_id or None if the job_id is not in the dockinginfo table."""
    rows = session.execute("SELECT job_id FROM dockinginfo WHERE job_id = :x", {'x': job_id}).all()
    if len(rows) < 1:
        return None
    assert len(rows) == 1, 'Non-unique job_id'
    return rows[0]['job_id']


def get_result_id(canonical_smiles, pdb_id, pose_id, pose_location, session): #, pdb_id, pose_location, pose_id
    """Returns the result_id or None if the result_id is not in the results table."""
    rows = session.execute(
    "SELECT result_id FROM results "
    "  WHERE canonical_smiles = :canonical_smiles AND "
    "        pdb_id = :pdb_id AND"
    "        pose_id = :pose_id AND"
    "        pose_location = :pose_location",
    {
        'canonical_smiles': canonical_smiles,
        'pdb_id': pdb_id,
        'pose_id': int(pose_id),
        'pose_location': pose_location
    }).all()
    if len(rows) < 1:
        return None
    assert len(rows) == 1, 'Non-unique result_id'
    return rows[0]['result_id']
    

def insert_into_v2(table, values_dict, session): 
    """Inserts a dictionary of <column name> -> <value> values into the given table.

    This function constructs queries dynamically, so it should never be used in a web app
    or any other place where a user may supply untrusted input.

    The data types are changed from NA, '' and None to NULL supoorting PSQL table datatypes for activities;
    """
    columns = list(values_dict.keys())
    column_str = ', '.join(columns)
    value = list(values_dict.values())
    va =''
    for i,x in enumerate(value):
        if x is None:
            va += 'NULL' + ', '
        elif type(x) is not str:
            va += str(x) + ', '
        elif x == "NA":
            va += 'NULL' + ', '
        else:
            va += '\''  + x + '\'' + ', '
        nstr = re.sub(",\s+$", "",va)

    sql = "INSERT INTO {table} ({columns_str}) VALUES ({values_str})".format(
            table=table,
            columns_str=column_str,
            values_str=nstr)
    session.execute(sql)


def insert_into(table, values_dict, session):
    """Inserts a dictionary of <column name> -> <value> values into the given table.

    This function constructs queries dynamically, so it should never be used in a web app
    or any other place where a user may supply untrusted input.
    """

    columns = values_dict.keys()
    columns_str = ', '.join(columns)
    values_str = ', '.join(':' + col for col in columns)
    sql = "INSERT INTO {table} ({columns_str}) VALUES ({values_str})".format(
            table=table,
            columns_str=columns_str,
            values_str=values_str)

    session.execute(sql, clean_values_dict(values_dict))


def insert_into_no_param_substitution(table, values_dict, session):
    """Inserts a dictionary of <column name> -> <value> values into the given table.

    This function constructs queries dynamically, so it should never be used in a web app
    or any other place where a user may supply untrusted input.

    In constrast to insert_into(), this function uses python string formatting instead
    of sql parameter substitution. This makes it possible to insert dynamic values.
    When possible you should avoid using this method and use insert_into() instead.
    """
    columns = values_dict.keys()
    columns_str = ', '.join(columns)
    values_str = ', '.join(values_dict[col] for col in columns)
    sql = "INSERT INTO {table} ({columns_str}) VALUES ({values_str})".format(
            table=table,
            columns_str=columns_str,
            values_str=values_str)
    session.execute(sql)


def update(table, primary_key_field, primary_key_value, values_dict, session):
    """Updates a row corresponding to the given primary key with values in values_dict.

    This function constructs queries dynamically, so it should never be used in a web app
    or any other place where a user may supply untrusted input.
    """
    set_str = ', '.join('{} = :{}'.format(col, col) for col in values_dict)
    sql = "UPDATE {table} SET {set_str} WHERE {primary_key_field} = :primary_key_value".format(
        table=table,
        set_str=set_str,
        primary_key_field=primary_key_field,
    )
    sql_params = deepcopy(values_dict)
    sql_params['primary_key_value'] = primary_key_value
    session.execute(sql, clean_values_dict(sql_params))


def update_no_param_substitution(table, primary_key_field, primary_key_value, values_dict, session):
    """Updates a row corresponding to the given primary key with values in values_dict.

    This function constructs queries dynamically, so it should never be used in a web app
    or any other place where a user may supply untrusted input.

    In constrast to update(), this function uses python string formatting instead
    of sql parameter substitution. This makes it possible to update with dynamic values.
    When possible you should avoid using this method and use update() instead.
    """
    set_str = ', '.join('{} = {}'.format(col, val) for col, val in values_dict.items())
    sql = "UPDATE {table} SET {set_str} WHERE {primary_key_field} = {primary_key_value}".format(
        table=table,
        set_str=set_str,
        primary_key_field=primary_key_field,
        primary_key_value=primary_key_value,
    )
    session.execute(sql)


def row_count(filename):
    """Returns the number of rows in a given text file. Useful for progress bars."""
    row_count = 0
    with open(filename, 'r') as fp:
        for row in fp:
            row_count += 1
    return row_count - 1  # Assume there is a header row.


def na_to_none(value):
    if value == 'NA':
        value = None
    return value


def clean_values_dict(values_dict):
    """Changes every value that is 'NA' to None."""
    result_dict = {}
    for key in values_dict:
        result_dict[key] = na_to_none(values_dict[key])
    return result_dict
