"""
Loads data from docking result sof BTK and SSW (version?).

Expects the following columns:

"""


import pandas as pd
from tqdm import tqdm

from dock_db.environment import session_scope
#from dock_db.ids import new_pose_id
from dock_db.load import utils
from datetime import datetime, timezone

tstamp = datetime.now(timezone.utc)
time_added = tstamp.strftime("%Y-%m-%d %H:%M")

#Read data:
#Structures:
structures_df = pd.read_csv('/home/ec2-user/DockDB/data/AKT1_smina_acs_10jul2023_structures_table.csv')
print(structures_df.head())
structures_added = 0
structures_updated = 0
structures_updated_set = set()  # To make sure a structures is not updated more than once

# structures_df = pd.read_excel('/Users/guru/Guru/Projects/DB/dockdb/data/DockDB_SSW_BTK.csv', sheet_name='Structures')
 #--- Insert into or update structure ---#
for index, row in tqdm(structures_df.iterrows(), total=structures_df.shape[0]):
    with session_scope() as session:
        structure_id = utils.get_structure_id(row['pdb_id'],row['structure_type'], row['structure_location'], session)
        structures_values_dict = {
            'pdb_id': row['pdb_id'],
            'uniprot_id': row['uniprot_id'],
            'optimized_method': row['optimized_method'],
            'cmp_hd_id': row['cmp_hd_id'],
            'structure_location': row['structure_location'],
            'structure_type' : row['structure_type'],
            'record_added': time_added,
        }
        if hasattr(row, 'structure_comment'):
            structures_values_dict['structure_comment'] = row['structure_comment']  
        if structure_id is not None and structure_id not in structures_updated_set:
            print(structure_id)
            utils.update('structures', 'structure_id', structure_id, structures_values_dict, session)
            structures_updated += 1
            structures_updated_set.add(structure_id)
        else:
            utils.insert_into('structures', structures_values_dict, session)
            structures_added += 1
            structures_updated_set.add(structure_id)
print('Structures added:'+str(structures_added))
print('Structures updated:'+str(structures_updated))


#Compounds
compounds_df = pd.read_csv('/home/ec2-user/DockDB/data/AKT1_smina_acs_10jul2023_compounds_table.csv')
print(compounds_df.head())
compounds_added = 0
compounds_updated = 0
compounds_updated_set = set() 
#--- Insert into or update compounds ---#
for index, row in tqdm(compounds_df.iterrows(), total=compounds_df.shape[0]):
    with session_scope() as session:
        cmp_hd_id = utils.get_cmp_hd_id(row['cmp_hd_id'], row['cmp_location'], session)
        compounds_values_dict = {
            'cmp_hd_id': row['cmp_hd_id'],
            'cmp_type': row['cmp_type'],
            'canonical_smiles': row['canonical_smiles'],
            'cmp_location': row['cmp_location'],
            'record_added': time_added,
        }
        if hasattr(row, 'cmp_comment'):
            compounds_values_dict['cmp_comment'] = row['cmp_comment']  
        if cmp_hd_id is not None and cmp_hd_id not in compounds_updated_set:
            utils.update('compounds', 'cmp_hd_id', cmp_hd_id, compounds_values_dict, session)
            compounds_updated += 1
            compounds_updated_set.add(cmp_hd_id)
        else:
            utils.insert_into('compounds', compounds_values_dict, session)
            compounds_added += 1
            compounds_updated_set.add(cmp_hd_id)
print('Compounds added:'+ str(compounds_added))
print('Compounds updated:'+ str(compounds_updated))

#Decoys:
decoys_df = pd.read_csv('/home/ec2-user/DockDB/data/AKT1_smina_acs_10jul2023_decoys_table.csv')
decoys_added = 0
decoys_updated = 0
decoys_updated_set = set()

for index, row in tqdm(decoys_df.iterrows(), total=decoys_df.shape[0]):
    with session_scope() as session:
        #--- Insert into or update decoys
        decoy_id = utils.get_decoy_id(row['decoy_smiles'], row['decoys_location'], session)
        decoys_values_dict = {
            'cmp_hd_id': row['cmp_hd_id'],
            'decoy_smiles': row['decoy_smiles'],
            'decoy_method': row['decoy_method'],
            'decoy_location': row['decoys_location'],
            'record_added': time_added
        }
        if hasattr(row, 'decoy_comment'):
            decoys_values_dict['cmp_comment'] = row['decoy_comment'] 
        if decoy_id is not None and decoy_id not in decoys_updated_set:
            utils.update('decoys', 'cmp_hd_id', cmp_hd_id, decoys_values_dict, session)
            decoys_updated += 1
            decoys_updated_set.add(decoy_id)
        else:
            utils.insert_into('decoys', decoys_values_dict, session)
            decoys_added += 1
            decoys_updated_set.add(decoy_id)
print('Decoys added:'+ str(decoys_added))
print('Decoys added:'+ str(decoys_updated))


#Docking info:
docking_df = pd.read_csv('/home/ec2-user/DockDB/data/AKT1_smina_acs_10jul2023_dockinginfo_table.csv')
jobs_added = 0
jobs_updated_set = set()
for index, row in tqdm(docking_df.iterrows(), total=docking_df.shape[0]):
    tstamp_job_id = row['job_id']
    with session_scope() as session:
        #--- Insert into or update decoys
        job_id = utils.get_job_id(row['job_id'], session)		
        docking_values_dict = {
            'job_id': row['job_id'],
            'bs_location': row['bs_location'],
            'docking_parameters': row['docking_parameters'],
            'results_location': row['results_location'],
            'bs_type': row['bs_type'],
            'record_added': time_added
        }
        if hasattr(row, 'docking_comment'):
            docking_values_dict['docking_comment'] = row['docking_comment'] 
        if job_id is not None and job_id not in jobs_updated_set:
            print('Job exits! please provide a new job id with timestamp')
        else:
            utils.insert_into('dockinginfo', docking_values_dict, session)
            jobs_added += 1
            jobs_updated_set.add(job_id)
print('Jobs added:'+str(jobs_added))

#Results:
results_df = pd.read_csv('/home/ec2-user/DockDB/data/AKT1_smina_acs_10jul2023_results_table.csv')
results_added = 0
results_updated_set = set()
for index, row in tqdm(results_df.iterrows(), total=results_df.shape[0]):
    with session_scope() as session:
        #--- Insert into or update decoys
        result_id = utils.get_result_id(row['canonical_smiles'],row['pdb_id'],row['pose_id'],row['pose_location'], session)
        result_values_dict = {
            'job_id': tstamp_job_id,
            'pdb_id': row['pdb_id'],
            'pose_id': row['pose_id'],
            'cmp_id': row['cmp_id'],
            'canonical_smiles': row['canonical_smiles'],
            'docking_score': row['docking_score'],
            'pose_location': row['pose_location'],
            'dock_log_location': row['dock_log_location'],
            'record_added': time_added
        }
        if hasattr(row, 'result_comment'):
            result_values_dict['result_comment'] = row['result_comment'] 
        if result_id is not None and result_id not in results_updated_set:
            print('Job exits! please provide a new job id with timestamp')
        else:
            utils.insert_into('results', result_values_dict, session)
            results_added += 1
            results_updated_set.add(result_id)
print('Results added:'+str(results_added))
