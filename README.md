# Welcome to the DockDB  gitlab repo! 
DockDB is a framework that serves as an all-inclusive Relational Database Management System (RDBMS) providing users a computational infrastructure to manage and access a database dedicated for docking studies. 


# Connecting to the database
To connect to the DockDB, there are two high-level steps: (1) establish an SSH connection to the host, and (2) connect to the database using Postgres client such as `psql` or DbVisualizer


### Connect to the host
This step will depend on where your production version of the DockDB is hosted. For details about how to create a psql server hosted, see the dockdb-admin repo. 


### Accessing via a Postgres client
Once you have connected to the host via ssh, we can now run a postgres client locally (**not on the host**) to connect to the database using the following connection parameters:

- Database server: `localhost`
- Database port: `5553`
- Database: `postgres`
- User: `<<Your hd-database user name>>`
- Password: `<<Your hd-database password>>`

How those parameters will be used depends on your preferred Postgres client. For example, if you are using the command-line client `psql`, you would connect by running the following command **on your local machine**:

```
psql -h localhost -U <<Your database user name>> -p 5553 -d postgres -W
```

Here, you will now be able to view the contents of your database! 


# Running bulk extracts
## With psql

If you want to extract information from the database, an easy way to do it is via the `psql` command-line tool.

For example, suppose you wanted to extract all inhibitors of one of the proteins in the database with a given substructure. This could be captured by the following query:

```
SELECT
cmp_id,
FROM compounds;
```

We can run this query interactively within the psql environment. Alternatively, we can automate this process by copying and pasting this query to a file (say compounds.sql) and run the following from the shell (assuming you’ve connected to the bastion host):
```
psql -h localhost -U <<your username>> -p 5553 -d postgres -f compounds.sql --csv -W > out.csv
```
We can script together several of these commands if there are several extracts we want to run.


## With python
Equivalently, we can run a similar query within python. We can access the database from any Python script by installing the `dockdb` pip package. We have prepared a jupyter notebook with an example on how to extract and analyze the data using python.


## Uploading from a new data source
If we are uploading from a new data source, you’ll want to write a new script in the load subdirectory. We can follow the example of the other scripts in this directory. Even if the new data source is similar to an existing data source, it’s usually best to create a new script. (We can copy the existing load_data_example script and modify it.)

We suggest, that any new ingestion scripts you write should be pushed to git to ensure that the repository is up to date.


## Testing uploads locally
When developing upload scripts, it’s helpful to test them out on a local version of the database.

From the dockdb repository root directory, run

```
export DOCK_DATABASE_URL=postgresql://postgres@localhost:5533/postgres
```

This tells the code inside the repo that we want to connect to the local database (which the Docker container exports onto port 5533 on your machine).

We can put any input data files inside the `data/` directory, which is ignored by git. Then we can test our scripts locally using commands such as

```
python -m dock_db.load.load_data_example data/your_data.csv --allow_update
```

As we build and test the scripts, it will often be helpful to reset the database. We can do that from the root of the `hd-dockdb` repository and entering the following commands:

```
./alembic_local downgrade base
./alembic_local upgrade head
```

**Warning**: make sure to type ./alembic_local and not ./alembic_prod, as the latter will make production changes.

If there are any issues with the above two commands, we can also stop and restart the Docker container.


## Running uploads to prod

After we have tested the upload locally, we can run it in production.

**Warning**: it is generally a good idea to create a snapshot before making any major changes to the production database.

First, let's connect to the host. Then run

```
export DOCK_DATABASE_URL=postgresql://<<your username>>:<<your password>>@localhost:5553/postgres
```

Finally, let's run your upload script, e.g.:

```
python -m dock_db.load.load_data_example data/your_data.csv --allow_update
```